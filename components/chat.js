import React, {useState, useEffect} from "react";
import socketIOClient from "socket.io-client";
import {GlobalStyle} from "./styled/GlobalStyle";
import Contact from "./contact";
import styled from 'styled-components';
import Message from "./message";


const All = styled.div`

.align-left {
  text-align: left;
}

.align-right {
  text-align: right;
}
.float-right {
  float: right;
}
.clearfix:after {
visibility: hidden;
display: block;
font-size: 0;
content: " ";
clear: both;
height: 0;
}

 `;
const Container = styled.div`
margin: 0 auto;
  width: 750px;
  background: #444753;
  border-radius: 5px;
`;
const PeopleList = styled.div` 
  width:260px;
  height: 300px;
  float: left;
  .search {
  }
  input::placeholder{
  color:white;
  }
`;

const Search = styled.div`
     padding: 20px;
`;
const DivContList = styled.div`
    height: 444px;
    overflow-y: scroll;
    ::-webkit-scrollbar {display: none;}

`;
const UlListContacts = styled.ul`
 height: 300px;
  list-style: none;
    padding: 20px;
`;

const SearchInput = styled.input`
 border-radius: 3px;
    border: none;
    padding: 14px;
    color: white;
    background: #6A6C75;
    width: 90%;
    font-size: 14px;
`;
const Li = styled.li`
padding-bottom: 20px;`;

const DivChat = styled.div`
  width: 490px;
  float:left;
  background: #F2F5F8;
  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;
  color: #434651;
`;
const ChatHeader = styled.div`
  padding: 20px;
    border-bottom: 2px solid white;
`;

const HeaderChatAbaut = styled.div`
      float: left;
      padding-left: 10px;
      margin-top: 6px;
`;
const ChatWidth = styled.div`
  font-weight: bold;
      font-size: 16px;
`;

const LogOut = styled.div``;
const A = styled.a`
padding-left: 10px;
    margin-top: 6px;
    float: right;
`;
const ChatHistory = styled.div`
padding: 30px 30px 20px;
    border-bottom: 2px solid white;
    overflow-y: scroll;
    height: 300px;
     
`;
const UlChat = styled.ul``;

const ChatMessage = styled.div`
 padding: 30px;
`;
const TexArea = styled.textarea`
     width: 100%;
      border: none;
      padding: 10px 20px;
      font: 14px/22px "Lato", Arial, sans-serif;
      margin-bottom: 10px;
      border-radius: 5px;
      resize: none;;
`;
const ENDPOINT = "https://node-chat-boxitas.herokuapp.com/";
const socket = socketIOClient(ENDPOINT);

function Chat(props) {
    console.log("Lista usuarios pasados por props: " + props.list);

    const [user, setUser] = useState({
        usersList: props.list === 'undefined' ? [] : props.list
    });
    console.log("Lista usuarios : " + user.usersList);
    const [msg, setMsg] = useState("");
    const [recMsg, setRecMsg] = useState({
        listMsg: []
    });
    const [loggedUser, setLoggedUser] = useState(props.user);
    const [search, setSearch] = useState("");
    useEffect(() => {
        // list of connected users
        console.log("useEffect");
        socket.on("users", data => {
            console.log("user list: " + user.usersList);
            setUser({usersList: JSON.parse(data)})

        });
        // we get the messages
        socket.on("getMsg", data => {
            let listMessages = recMsg.listMsg;
            listMessages.push(JSON.parse(data));
            console.log("mesages: " + JSON.parse(data));
            setRecMsg({listMsg: listMessages});
        });
        return () => {
            socket.emit('disconnect');
        }

    }, []);

    const onChangeState = e => {
        e.preventDefault();
        console.log(e.target.value);
        setMsg(e.target.value);
    };

    // to send a message
    const sendMessage = e => {
        if (e.keyCode === 13 && !e.shiftKey) {
            console.log("loggedUser: " + loggedUser);
            socket.emit("sendMsg", JSON.stringify({id: loggedUser.id, msg: msg}));
            setMsg("");
        }


    };

    const onLogOut = e => {
        socket.emit("disconnect");

    };

    const onChageBuscar = e => {
        setSearch(e.target.value);
    };


    return (
        <All>
            <Container className="container clearfix">
                <GlobalStyle/>
                <PeopleList className="people-list" id="people-list">
                    <Search className="search">
                        <SearchInput type="text" value={search} onChange={onChageBuscar} placeholder="buscar"/>
                    </Search>
                    <DivContList>
                        <UlListContacts className="list">
                            {user.usersList.filter(user => user.userName.includes(search)).map((user, index) => {
                                return (<Li className="clearfix" key={index}> <Contact user={user}/></Li>)
                            })}
                        </UlListContacts>
                    </DivContList>
                </PeopleList>
                <DivChat className="chat">
                    <ChatHeader className="chat-header clearfix">
                        <HeaderChatAbaut className="chat-about">
                            <ChatWidth className="chat-with">{loggedUser.userName}</ChatWidth>
                        </HeaderChatAbaut>
                        <LogOut className="chat-logOut">
                            <A className="logOut" onClick={onLogOut} href="/">Salir</A>
                        </LogOut>
                    </ChatHeader>
                    <ChatHistory className="chat-history">
                        <UlChat>
                            {recMsg.listMsg.map((msg, index) => {
                                if (msg.userName === loggedUser.userName) {
                                    return (
                                        <li key={index} className="clearfix">
                                            <Message msg={msg} other={0}/>
                                        </li>
                                    )
                                }
                                return (<li key={index}>
                                    <Message msg={msg} other={1}/>
                                </li>)
                            })}
                        </UlChat>

                    </ChatHistory>
                    <ChatMessage className="chat-message clearfix">
                        <TexArea name="message-to-send" value={msg} onKeyUp={sendMessage} onChange={onChangeState}
                                 id="message-to-send" placeholder="Escriba su mensaje" rows="3"/>
                    </ChatMessage>
                </DivChat>
            </Container>
        </All>
    );
}

export default Chat;