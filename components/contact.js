import React from "react";
import styled from 'styled-components';

export default function Contact(props) {


    const About = styled.div`
        float: left;
    margin-top: 8px;
    color: white;
        padding-left: 8px;
    `;
    const Name = styled.div`
    
    `;
    const Status = styled.div`
    color: #92959E;
    `;
    const I = styled.div``;


    return (

        <About className="about">
            <Name className="name">{props.user.userName}</Name>
            <Status className="status">
                <I className="fa fa-circle online"/> online
            </Status>
        </About>
    );
}