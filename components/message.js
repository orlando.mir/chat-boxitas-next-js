import React from "react";
import styled from 'styled-components';






const Container=styled.div`
.my-message {
      background: #86BB71;
    }
        .other-message {
      background: #94C2ED;
      
      &:after {
        border-bottom-color: #94C2ED;
        left: 93%;
      }
    }
`;
const MessageData=styled.div`

    margin-bottom: 15px;
`;
const MessageT=styled.span`
padding-left: 6px;
`;
const MessageNAme=styled.span`

`;
const MessageDiv=styled.div`
      
      color: white;
      padding: 18px 20px;
      line-height: 26px;
      font-size: 16px;
      border-radius: 7px;
      margin-bottom: 30px;
      width: 90%;
      position: relative;
      
      &:after {
        bottom: 100%;
        left: 7%;
        border: solid transparent;
        content: " ";
        height: 0;
        width: 0;
        position: absolute;
        pointer-events: none;
        border-bottom-color: #86BB71;
        border-width: 10px;
        margin-left: -10px;
      }
    
`;

export default function Message(props) {


  if(props.other===0){
      return (
          <Container>
              <MessageData className="message-data align-right">
                  <MessageT className="message-data-time">{props.msg.time}</MessageT>
                  <MessageNAme className="message-data-name">{props.msg.userName}</MessageNAme>
              </MessageData>
              <MessageDiv className="message other-message float-right">
                  {props.msg.msg}
              </MessageDiv>
          </Container>
      )
  }else {
      return (
          <Container>
              <MessageData className="message-data align-left">
                  <MessageT className="message-data-time">{props.msg.time}</MessageT>
                  <MessageNAme className="message-data-name">{props.msg.userName}</MessageNAme>
              </MessageData>
              <MessageDiv className="message my-message float-left">
                  {props.msg.msg}
              </MessageDiv>
          </Container>
      )
  }

}